<?php 
/*
Template Name: Job Page
*/
  get_header();?>
  <?php if (have_posts()) : while (have_posts()) : the_post(); 
  		$language_information = wpml_get_language_information(get_the_id());
		$page_locale = $language_information['locale'];
		
  ?>
			<div class="subpage-name-header">
				<h1><?php the_title(); ?></h1>
				<span><?php the_field('description_content'); ?></span>
			</div>
			
		</header>
		<div class="cus1-main">
			<div class="header-shadow"></div>
			<div class="container">
				<?php 
					if ($page_locale == 'zh_CN') {
						echo '<h2>加入我们的团队</h2>';
					} else {
						echo '<h2>JOIN OUR TEAM</h2>';
					}
				?>
				<div class="underline"></div>
				<div class="row mar-div-30 join-our-team">
					<div class="col-md-5 left">
						<?php echo the_content();?>
					</div>
					<div class="col-md-7 right">
						<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
							<?php 

								global $post;
								$args = array('post_type' => 'job');
								$custom_posts = get_posts($args);

								$collapseName = array(
									1 => 'collapseOne', 
									2 => 'collapseTwo', 
									3 => 'collapseThree', 
									4 => 'collapseFour', 
									5 => 'collapseFive', 
									6 => 'collapseSix', 
									7 => 'collapseSeven', 
									8 => 'collapseEight', 
									9 => 'collapseNine', 
									10 => 'collapseTen', 
									);
								$groupHeading = array(
									1 => 'headingOne', 
									2 => 'headingTwo', 
									3 => 'headingThree', 
									4 => 'headingFour', 
									5 => 'headingFive', 
									6 => 'headingSix', 
									7 => 'headingSeven', 
									8 => 'headingEight', 
									9 => 'headingNine', 
									10 => 'headingTen', 
									);
								$index = 1;
								foreach($custom_posts as $post) : setup_postdata($post);
									$post_language = wpml_get_language_information(get_the_id());
									$post_locale = $post_language['locale'];
									if ($page_locale == $post_locale) {
							?>
										<div class="panel panel-default">
									    	<div class="panel-heading" role="tab" id="<?php echo $groupHeading[$index]; ?>">
									      		<h4 class="panel-title">
									        	<a data-toggle="collapse" data-parent="#accordion" href="#<?php echo $collapseName[$index]; ?>" aria-expanded="true" aria-controls="collapseOne">
									          		<?php the_title();?>
									        	</a>
									      		</h4>
								    		</div>
								    		<div id="<?php echo $collapseName[$index]; ?>" class="panel-collapse collapse in" role="tabpanel" aria-labelledby="<?php echo $groupHeading[$index]; $index ++; ?>">
									      		<div class="panel-body">
									        		<?php the_content(); ?>
									      		</div>
									    	</div>
									  	</div>
							<?php 
									}
								endforeach;
								wp_reset_postdata();
							?>
						  	
						</div>
					</div>
				</div>
			</div>
		</div>
<?php endwhile; endif; ?>
<?php get_footer(); ?>
