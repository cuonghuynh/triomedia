
<?php 
/*
Template Name: Latest News Page
*/
  get_header();?>
  <div class="subpage-name-header">
        <h1><?php the_title(); ?></h1>
        <span><?php the_field('description_content'); ?></span>
      </div>
    </header>
    <div class="cus1-main">
      <div class="header-shadow"></div>
      <div class="container">

<?php
  $posts_per_page = get_option('posts_per_page');
  $paged = 1;
  if(get_query_var('paged')){
    $paged = get_query_var('paged');
  }elseif(get_query_var('page')){
    $paged = get_query_var('page');
  }
  $args = array(
    'post_type' => array('post'),
    'pagination'  => true,
    'posts_per_page' => $posts_per_page,
    'paged' => $paged,
    'order' => 'desc', );
  $query =  new WP_Query($args);
  $counter = 1;
?>

    
    <?php if ($query->have_posts()) : ?>
        <div class="list-post" class="js-masonry" data-masonry-options='{ "columnWidth": 360, "itemSelector": ".item" }'>
            <?php while($query->have_posts()) : $query->the_post(); 
            //get thumbnail url
              $thumb_id = get_post_thumbnail_id();
              $thumb_url = wp_get_attachment_image_src($thumb_id,'thumbnail-size', true);

            //get comment information
              $comments_count = wp_count_comments();
              $cmmt = "No comment";
              if($comments_count->total_comments > 0){
                $cmmt = $comments_count->total_comments;
              }
              $video = get_field('video');
              $gallery = get_field('gallery'); 

            ?>
            <div class="item">
                
                <?php if($gallery) { ?>
                <div class="item-main">
                  <div id="wowslider-container1" class="wowslider-container">
                    <div class="ws_images">
                      <ul>
                        <?php foreach ($gallery as $image) : ?>
                        <li>
                          <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="" title="" id=""/>
                        </li>
                        <?php endforeach; ?>
                      </ul>
                    </div>
                    <span class="wsl"></span>
                    <div class="ws_shadow"></div>
                  </div>  
                </div>
                <?php } elseif($video) { 

                  
                                    ?>
                <div class="item-main video"  data-src="<?php echo get_field('video');?>">
                </div>
                <?php } else { ?>
                <div class="item-main">
                  <?php if(has_post_thumbnail()) { ?>
                  <img src="<?php echo $thumb_url[0]; ?>" />
                  <?php } ?>
                </div>
                <?php } ?>
                <div class="info">
                  <p class="highlight">
                    <span class="fa fa-user"></span> <?php the_author(); ?>
                    | <span class="fa fa-comments"></span> <?php echo $cmmt; ?>
                    | <span class="fa fa-calendar"></span> <?php echo get_the_date('d.m.Y'); ?>
                  </p>
                  <a href="<?php the_permalink(); ?>" title=""><h4><?php the_title(); ?></h4></a>
                  <?php the_excerpt(); ?>
                  <?php 
                      $tags = get_the_tags();
                      if ($tags) { ?>
                        <hr />
                        <div class="row tag">
                          <div class="col-md-12"><i class="fa fa-tag"></i> 
                              <?php
                              $i = 0 ;
                              foreach($tags as $tag) {
                                $i++;
                                echo $tag->name;
                                if($i < count($tags)){
                                  echo ', '; 
                                }
                              }  ?>
                            <div class="right"> 
                              <i class="fa fa-share-alt"></i>
                            </div>
                          </div>
                        </div>
                    <?php } ?>
                </div>
            </div>
        <?php endwhile; wp_reset_postdata(); ?>
        </div>
        <div class="paginator text-center">
            <?php 
              $big = 999999;
              echo paginate_links( array(
                'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),   
                'type' => 'list',
                'format' => '?paged=%#%',
                'total' =>$query->max_num_pages,
                'prev_text'=>'Previous',
                'next_text'=>'Next',
            ) );

            ?>
        </div>
    <?php else : ?>
      <h1>No News</h1>
    <?php endif; ?>
      
    </div>

<?php get_footer(); ?>


