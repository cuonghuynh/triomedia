<!DOCTYPE html>

<html <?php language_attributes(); ?>>

<head>
  <meta charset="<?php bloginfo('charset'); ?>" />
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=10"/>
  
  <?php if (is_search()) { ?>
    <meta name="robots" content="noindex, nofollow" />
  <?php } ?>

  <title>
   <?php
      if (get_the_id() == 37) {
        echo 'Home ';
      }
      if (get_the_id() == 351) {
        echo '主页 ';
      }
      $current = 'subpage-header';
      if (function_exists('is_tag') && is_tag()) {
         single_tag_title("Tag Archive for &quot;"); echo '&quot; - '; }
      elseif (is_archive()) {
         wp_title(''); echo ' Archive - '; }
      elseif (is_search()) {
         echo 'Search for &quot;'.wp_specialchars($s).'&quot; - '; }
      elseif (!(is_404()) && (is_single()) || (is_page())) {
         wp_title(''); echo ' - '; }
      elseif (is_404()) {
         echo 'Not Found - '; }
      if (is_home()) {
         bloginfo('name'); echo ' - '; bloginfo('description'); $current = 'home-header'; }
      else {
          bloginfo('name'); }
      if ($paged>1) {
         echo ' - page '. $paged; }
   ?>
  </title>

  <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
  <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
  <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_url'); ?>/css/bootstrap.min.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_url'); ?>/css/wow_slider.css">
  <link rel="stylesheet" type="text/css" href="<?php echo get_bloginfo('template_url'); ?>/css/font-awesome.css">
  <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" />

  <?php if ( is_singular() ) wp_enqueue_script( 'comment-reply' ); ?>

  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
  <div class="wrapper">
    <header class="<?php echo $current; ?>">
      <div class="container">
        <div class="row top">
          <nav class="navbar navbar-default">
                <div class="container-fluid">
                  <div class="navbar-header">
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                      <span class="sr-only"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                      <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand logo" href="<?php echo home_url(); ?>"><img class="img-responsive" src="<?php echo get_bloginfo('template_url'); ?>/images/logo.png" /></a>
                  </div>
                  <div id="navbar" class="navbar-collapse collapse">
                    <?php
                      $menu_name      = 'main_nav';
                      $locations      = get_nav_menu_locations();
                      $menu           = wp_get_nav_menu_object( $locations[ $menu_name ] );
                      $menu_items     = wp_get_nav_menu_items( $menu->term_id );
                      //wp_nav_menu(array('menu' => 'main_nav', 'menu_class' => 'nav navbar-nav navbar-right menu'));
                    ?>
                    <ul class="nav navbar-nav navbar-right menu">
                        <?php
                          foreach ( (array) $menu_items as $key => $menu_item) {
                            echo '<li><a href="' . $menu_item->url . '">' . $menu_item->title . '</a></li>';
                          }
                          $languages = icl_get_languages('skip_missing=1');
                        ?>
                        <li><a href="<?php echo $languages['en']['url']; ?>">ENGLISH</a></li>
                        <li><a href="<?php if (isset($languages['zh-hans'])) { echo $languages['zh-hans']['url']; } else { echo '#'; } ?>">| 中文</a></li>
                    </ul> -->
                  </div><!--/.nav-collapse -->
                </div><!--/.container-fluid -->
              </nav>
        </div>
      </div>
     