<?php get_header(); ?>

  <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
      <div class="subpage-name-header">
        <h1><?php the_title(); ?></h1>
        <span><?php the_field('description_content'); ?></span>
      </div>
    </header>
    <div class="cus1-main">
      <div class="header-shadow"></div>
      <div class="container">
        <?php 
          $video = get_field('video');
          $gallery = get_field('gallery'); 
          if ($gallery) { ?>
          <div class="media-holder">
            <div id="wowslider-container1" class="wowslider-container">
              <div class="ws_images">
                <ul>
                  <?php foreach ($gallery as $image) : ?>
                  <li>
                    <img src="<?php echo $image['sizes']['thumbnail']; ?>" alt="" title="" id=""/>
                  </li>
                  <?php endforeach; ?>
                </ul>
              </div>
              <span class="wsl"></span>
              <div class="ws_shadow"></div>
            </div>
          </div>
          <?php }
            if ($video) { ?>
            <div class="media-holder">
               <div class="video"  data-src="<?php echo $video;?>">
              </div>
            </div>
            <?php }
          the_content(); 
        ?>
      </div>
    </div>

  <?php endwhile; endif; ?>

<?php get_footer(); ?>


