<?php
/*
Template Name: Cover Home Page
*/

	get_header(); 

	$language_information = wpml_get_language_information(get_the_id());
	$page_locale = $language_information['locale'];
		
?>
	<div class="banner-home">
		<div class="banner">
			<ul>
			<?php
			if ( function_exists( 'ot_get_option' ) ) {
				$sliders = ot_get_option( 'silders' );
				if (count(sliders) > 0) {
					foreach ($sliders as $slider) {
						echo '<li><img src="' . $slider['image'] . '" alt="Banner Home" /></li>';
					}
					
				}
			}

			?>
				</ul>
		</div>
	</div>
	<div class="banner-home-shadow"><img src="<?php echo get_bloginfo('template_url'); ?>/images/banner-home-shadow.jpg" /></div>
	</header>
	<div class="main home-content">
			<div class="container text-center">
				<h2><?php the_field('introduction_header'); ?></h2>
				<div class="underline center"></div>
				<div class="row gallery gallery-2">
					<div class="col-lg-8 col-md-8 col-sm-8">
						<div class="show-read-more">
							<div class="read-more"><span><?php the_field('distribution_title'); ?></span></div>
							<img class="img-responsive" src="<?php the_field('distribution_image') ?>" />
						</div>
					</div>
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="show-read-more">
							<div class="read-more"><span><?php the_field('post_production_title'); ?></span></div>
							<img class="img-responsive" src="<?php the_field('post_production_image') ?>" />
						</div>
					</div>
				</div>
				<div class="row gallery gallery-3">
					<div class="col-lg-4 col-md-4 col-sm-4">
						<div class="show-read-more">
							<div class="read-more"><span><?php the_field('equipment_rental_title'); ?></span></div>
							<img class="img-responsive" src="<?php the_field('equipment_rental_image') ?>" />
						</div>
					</div>
					<div class="col-lg-5 col-md-5 col-sm-5">
						<div class="show-read-more">
							<div class="read-more"><span><?php the_field('production_support_title'); ?></span></div>
							<img class="img-responsive" src="<?php the_field('production_support_image') ?>" />
						</div>
					</div>
					<div class="col-lg-3 col-md-3 col-sm-3">
						<div class="show-read-more">
							<div class="read-more"><span><?php the_field('studio_title'); ?></span></div>
							<img class="img-responsive" src="<?php the_field('studio_image') ?>" />
						</div>
					</div>
				</div>
				<div class="mar-100"></div>
				<!-- project gallery place -->
				<?php include (TEMPLATEPATH . '/inc/project-gallery.php' ); ?>
			</div>
		</div>
		<?php the_content(); ?>
		<!-- project gallery place -->
		<?php include (TEMPLATEPATH . '/inc/our-producers.php' ); ?>

<?php get_footer(); ?>
