<?php
	// Add RSS links to <head> section
	automatic_feed_links();

	// UNCOMMENT THE FOLLOWING IF YOU NEED JQUERY LOADED:
	/*
	if ( !is_admin() ) {
		 wp_deregister_script('jquery');
		 wp_register_script('jquery', ("http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"), false);
		 wp_enqueue_script('jquery');
	}
	*/

	// Clean up the <head>
	function removeHeadLinks() {
		remove_action('wp_head', 'rsd_link');
		remove_action('wp_head', 'wlwmanifest_link');
	}

	add_action('init', 'removeHeadLinks');
	remove_action('wp_head', 'wp_generator');

	if (function_exists('register_sidebar')) {
		register_sidebar(array(
			'name' => 'Sidebar Widgets',
			'id'   => 'sidebar-widgets',
			'description'   => 'These are widgets for the sidebar.',
			'before_widget' => '<div id="%1$s" class="widget %2$s">',
			'after_widget'  => '</div>',
			'before_title'  => '<h1>',
			'after_title'   => '</h1>'
		));
	}

	function blankhtml5_comment($comment, $args, $depth) {
		$GLOBALS['comment'] = $comment; ?>
		<li <?php comment_class(); ?> id="li-comment-<?php comment_ID() ?>">
			<article id="comment-<?php comment_ID(); ?>">
				<header class="comment-author vcard">
					<?php echo get_avatar($comment,$size='48',$default='<path_to_url>' ); ?>

					<?php printf(__('<cite class="fn">%s</cite> <span class="says">says:</span>'), get_comment_author_link()) ?>
					<?php if ($comment->comment_approved == '0') : ?>
						<em><?php _e('Your comment is awaiting moderation.') ?></em><br />
					<?php endif; ?>

					<div class="comment-meta commentmetadata"><a href="<?php echo htmlspecialchars( get_comment_link( $comment->comment_ID ) ) ?>"><?php printf(__('%1$s at %2$s'), get_comment_date(),  get_comment_time()) ?></a><?php edit_comment_link(__('(Edit)'),'  ','') ?></div>
				</header>

				<?php comment_text() ?>

				<div class="reply">
					<?php comment_reply_link(array_merge( $args, array('depth' => $depth, 'max_depth' => $args['max_depth']))) ?>
				</div>
			</article>
	<?php
	}

	//*******************************
	// Enable to show Post Thumbnail 
	//*******************************

	If(function_exists('add_theme_support')){
		Add_theme_support('post-thumbnails');
	}

	//*******************************
	// Additional Image Resize
	//*******************************

	If(function_exists('add_image_size')){
		add_image_size( 'project-thumb', 350, 220, true);
	}
	

	//*******************************
	// Create Custom Post Type: PROJECT
	//*******************************

	add_action( 'init', 'register_cpt_project');

	function register_cpt_project() {
		$labels = array( 
		'name'               => __( 'Projects', 'text_domain' ),
		'singular_name'      => __( 'Single Project', 'text_domain' ),
		'add_new'            => __( 'Add New Project', '${4:Name}', 'text_domain' ),
		'add_new_item'       => __( 'Add New Project', 'text_domain}' ),
		'edit_item'          => __( 'Edit single Project', 'text_domain' ),
		'new_item'           => __( 'New single Project', 'text_domain' ),
		'view_item'          => __( 'View single Project', 'text_domain' ),
		'search_items'       => __( 'Search Project', 'text_domain' ),
		'not_found'          => __( 'No Project', 'text_domain' ),
		'not_found_in_trash' => __( 'No Project found in Trash', 'text_domain' ),
		'parent_item_colon'  => __( 'Parent single Project:', 'text_domain' ),
		'menu_name'          => __( 'Projects', 'text_domain' ),
		);

		$args = array(
			'labels'              => $labels,
			'hierarchical'        => false, 
			'public'              => true,
			'rewrite'             => array('slug' => 'project'),
			'show_ui'             => true,
			'show_in_menu'        => true,
			'publicly_queryable'  => true,
			'taxonomies'          => array( 'project_cat' ),
			'menu_position'       => 5,
			'menu_icon'           => 'dashicons-editor-video',
			'query_var'           => true,
			'capability_type'     => 'post', 
			'supports'            => array( 
										'title', 'editor', 'author', 'thumbnail', 
										'custom-fields'
									),
		);

		register_post_type( 'project', $args );
	}

	/**
	 *  Add custom taxonomy
	 *  for project custom post type
	 */

	function project_category() {
		 register_taxonomy(
			'project_cat',
			'project',
			array(
					'hierarchical' => true,
					'label' => 'Project Categories',
					'query_var' => true,
					'rewrite' => array('slug' => 'project_cat')
			)
		);
	}

	add_action( 'init', 'project_category' );

	//*******************************
	// Create Custom Post Type: JOB
	//*******************************

	add_action( 'init', 'register_cpt_job');

	function register_cpt_job() {
		$labels = array( 
		'name'               => __( 'Jobs', 'text_domain' ),
		'singular_name'      => __( 'Single Job', 'text_domain' ),
		'add_new'            => __( 'Add New Job', '${4:Name}', 'text_domain' ),
		'add_new_item'       => __( 'Add New Job', 'text_domain}' ),
		'edit_item'          => __( 'Edit single Job', 'text_domain' ),
		'new_item'           => __( 'New single Job', 'text_domain' ),
		'view_item'          => __( 'View single Job', 'text_domain' ),
		'search_items'       => __( 'Search Job', 'text_domain' ),
		'not_found'          => __( 'No Job', 'text_domain' ),
		'not_found_in_trash' => __( 'No Job found in Trash', 'text_domain' ),
		'parent_item_colon'  => __( 'Parent single Job:', 'text_domain' ),
		'menu_name'          => __( 'Jobs', 'text_domain' ),
		);

		$args = array(
			'labels'              => $labels,
			'hierarchical'        => false, 
			'public'              => true,
			'rewrite'             => array('slug' => 'job'),
			'show_ui'             => true,
			'show_in_menu'        => true,
			'publicly_queryable'  => true,
			'menu_position'       => 5,
			'menu_icon'           => 'dashicons-clipboard',
			'query_var'           => true,
			'capability_type'     => 'post', 
			'supports'            => array( 
										'title', 'editor', 'author', 'thumbnail', 
										'custom-fields'
									),
		);

		register_post_type( 'job', $args );
	}

	//*******************************
	// Create Custom Post Type: PRODUCER
	//*******************************

	add_action( 'init', 'register_cpt_producer');

	function register_cpt_producer() {
		$labels = array( 
		'name'               => __( 'Producers', 'text_domain' ),
		'singular_name'      => __( 'Single Producer', 'text_domain' ),
		'add_new'            => __( 'Add New Producer', '${4:Name}', 'text_domain' ),
		'add_new_item'       => __( 'Add New Producer', 'text_domain}' ),
		'edit_item'          => __( 'Edit single Producer', 'text_domain' ),
		'new_item'           => __( 'New single Producer', 'text_domain' ),
		'view_item'          => __( 'View single Producer', 'text_domain' ),
		'search_items'       => __( 'Search Producer', 'text_domain' ),
		'not_found'          => __( 'No Producer', 'text_domain' ),
		'not_found_in_trash' => __( 'No Producer found in Trash', 'text_domain' ),
		'parent_item_colon'  => __( 'Parent single Producer:', 'text_domain' ),
		'menu_name'          => __( 'Producers', 'text_domain' ),
		);

		$args = array(
			'labels'              => $labels,
			'hierarchical'        => false, 
			'public'              => true,
			'rewrite'             => array('slug' => 'producer'),
			'show_ui'             => true,
			'show_in_menu'        => true,
			'publicly_queryable'  => true,
			'menu_position'       => 5,
			'menu_icon'           => 'dashicons-video-alt',
			'query_var'           => true,
			'capability_type'     => 'post', 
			'supports'            => array( 
										'title', 'editor', 'author', 'thumbnail', 
										'custom-fields'
									),
		);

		register_post_type( 'producer', $args );
	}

	

	//*******************************
	// Register Menu
	//*******************************

	if(function_exists('register_nav_menus')){
		register_nav_menus(array( 'main_nav' => 'Main Navigation Menu'));
	}

	//*******************************
	// Include Custom Post Type in result of Category
	//*******************************

	function namespace_add_custom_types( $query ) {
		if( is_category()  || is_tag() && empty( $query->query_vars['suppress_filters'] ) ) {
			$query->set( 'post_type', array(
			 'post', 'page', 'nav_menu_item', 'project', 'producer', 'job'
			));
			return $query;
		}
	}
	add_filter( 'pre_get_posts', 'namespace_add_custom_types' );

	//*******************************
	// Custom Expert length
	//*******************************

	function new_excerpt_lenght($length){
		return 20;
	}

	add_filter('excerpt_length', 'new_excerpt_lenght');

	function new_excerpt_more($more){
		global $post;
		return '...';
	}
	add_filter('excerpt_more', 'new_excerpt_more');


	/**
	 * Required: set 'ot_theme_mode' filter to true.
	 */
	add_filter( 'ot_theme_mode', '__return_true' );

	/**
	 * Required: include OptionTree.
	 */
	require( trailingslashit( get_template_directory() ) . 'option-tree/ot-loader.php' );

	/**
	 * Theme Options
	 */
	require( trailingslashit( get_template_directory() ) . 'inc/theme-options.php' );

?>