    <footer>
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-sm-6 text-left">Copyright © 2014 Triomedia. </div>
					<div class="col-md-6 col-sm-6 text-right">
						<ul class="menu-social-footer">
						<?php
							if ( function_exists( 'ot_get_option' ) ) {
								$facebook 	= ot_get_option( 'facebook' );
								$gplus		= ot_get_option( 'gplus' );
								$twitter	= ot_get_option( 'twitter' );
								$flickr		= ot_get_option( 'flickr' );
								$pinterest 	= ot_get_option( 'pinterest' );
								if (!empty($facebook)) {
									echo '<li><a href="' . $facebook . '"><img src="' . get_bloginfo('template_url') . '/images/ico1-footer.png" /></a></li>';
								}

								if (!empty($flickr)) {
									echo '<li><a href="' . $flickr . '"><img src="' . get_bloginfo('template_url') . '/images/ico2-footer.png" /></a></li>';
								}

								if (!empty($gplus)) {
									echo '<li><a href="' . $gplus . '"><img src="' . get_bloginfo('template_url') . '/images/ico3-footer.png" /></a></li>';
								}

								if (!empty($pinterest)) {
									echo '<li><a href="' . $pinterest . '"><img src="' . get_bloginfo('template_url') . '/images/ico4-footer.png" /></a></li>';
								}

								if (!empty($twitter)) {
									echo '<li><a href="' . $twitter . '"><img src="' . get_bloginfo('template_url') . '/images/ico5-footer.png" /></a></li>';	
								}
							}							

						 ?>
						</ul>
					</div>
				</div>
			</div>
		</footer>
	</div>

  <?php wp_footer(); ?>

  <!-- Don't forget analytics -->

</body>

<script type="text/javascript" src="<?php echo get_bloginfo('template_url'); ?>/js/jquery-1.11.2.min.js"></script>
<script type="text/javascript" src="<?php echo get_bloginfo('template_url'); ?>/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo get_bloginfo('template_url'); ?>/js/masonry.js"></script>
<script type="text/javascript" src="<?php echo get_bloginfo('template_url'); ?>/js/isotope.pkgd.min.js"></script>
<script type="text/javascript" src="<?php echo get_bloginfo('template_url'); ?>/js/unslider.js"></script>
<script type="text/javascript" src="<?php echo get_bloginfo('template_url'); ?>/js/wowslider.js"></script>
<script type="text/javascript" src="<?php echo get_bloginfo('template_url'); ?>/js/wowslider.mod.js"></script>
<script type="text/javascript" src="<?php echo get_bloginfo('template_url'); ?>/js/app.js"></script>
</html>