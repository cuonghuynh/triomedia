<?php 
/*
Template Name: Cover Single Page
*/
get_header(); ?>
	<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="subpage-name-header">
				<h1><?php the_title(); ?></h1>
				<span><?php the_field('description_content'); ?></span>
			</div>
		</header>
		<div class="cus1-main">
			<div class="header-shadow"></div>
			<div class="container">
				<?php the_content(); ?>
			</div>
		</div>
	<?php endwhile; endif; ?>
<?php 
 get_footer(); 
?>