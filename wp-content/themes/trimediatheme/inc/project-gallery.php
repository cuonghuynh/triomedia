        <div class="group project-gallery-home">
          <h2><?php the_field('project_gallery_header'); ?></h2>
          <div class="underline center"></div>
          <div class="description color-1"><span><?php the_field('project_gallery_title'); ?></span></div>
          <div class="row list-gallery">
            <?php

              //get current page language
              $language_information = wpml_get_language_information( get_the_id() );
              $page_locale = $language_information['locale'];
              global $post;
              $args = array('post_type' => 'project', 'posts_per_page' => -1, 'order'=>'desc');
              $custom_posts = get_posts($args);

              $counter = 0;  

              foreach($custom_posts as $post) : setup_postdata($post); 

                 if ($counter > 5) {     // display post max is 3
                    break;
                }

                $thumb_id = get_post_thumbnail_id();
                $thumb_url = wp_get_attachment_image_src($thumb_id,'project-thumb', true);
                //get post language
                $post_language = wpml_get_language_information( get_the_id() );
                $post_locale = $post_language['locale'];

                if ( $page_locale == $post_locale ) {

                    $counter++;
              ?>
                <div class="col-lg-4 col-md-4 col-sm-4"><a href="<?php the_permalink(); ?>">
                  <img class="img-responsive medium" src="<?php echo $thumb_url[0]; ?>" />
                  <div class="title"><?php the_title(); ?></div>
                </a></div>
              <?php

                }

              endforeach;
              wp_reset_postdata();
            ?>
            
          
          </ul>
        </div>