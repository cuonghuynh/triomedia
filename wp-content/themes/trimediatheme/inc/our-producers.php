    <div class="main home-content">
      <div class="group container text-center">
        <div class="line-740"></div>
        <h2><?php the_field('our_producer_header'); ?></h2>
        <div class="underline center"></div>
        <div class="description color-1"><span><?php the_field('our_producer_title'); ?></span></div>
        <div class="producers">
          <div class="row list-producers">
          <?php
            //get current page language
            $language_information = wpml_get_language_information( get_the_id() );
            $page_locale = $language_information['locale'];

            global $post;
            $args = array('post_type' => 'producer', 'posts_per_page' => -1, 'order'=>'desc');
            $custom_posts = get_posts($args);

            $counter = 0;   // counting post to display
            
            foreach($custom_posts as $post) : setup_postdata($post);
               
                if ($counter > 2) {     // display post max is 3
                    break;
                }
                $fields = get_post_custom( get_the_id() );

                $thumb_id = get_post_thumbnail_id();
                $thumb_url = wp_get_attachment_image_src( $thumb_id,'thumbnail-size', true );
                
                //get post language
                $post_language = wpml_get_language_information( get_the_id() );
                $post_locale = $post_language['locale'];

                if ( $page_locale == $post_locale ) {
                     $counter++;    
                 ?>

                    <div class="col-lg-offset-3 col-md-offset-3 col-offset-md-12">
                      <div class="avatar"><img src="<?php echo $thumb_url[0]; ?>" /></div>
                      <div class="info">
                        <span class="name"><?php the_title(); ?></span></br>
                        <span class="position"><?php echo $fields['subtitle'][0]; ?></span>
                        <ul class="connect">
                          <?php if(!is_null($fields['fb-url'][0])) { ?>
                          <li><a class="icon fb-icon" href="<?php echo $fields['fb-url'][0]; ?>"></a></li>
                          <?php } ?>
                          <?php if(!is_null($fields['gplus-url'][0])) { ?>
                          <li><a class="icon gplus-icon" href="<?php echo $fields['gplus-url'][0]; ?>"></a></li>
                          <?php } ?>
                          <?php if(!is_null($fields['twitter-url'][0])) { ?>
                          <li><a class="icon twitter-icon" href="<?php echo $fields['twitter-url'][0]; ?>"></a></li>
                          <?php } ?>
                          <?php if(!is_null($fields['lin-url'][0])) { ?>
                          <li><a class="icon linkedin-icon" href="<?php echo $fields['lin-url'][0]; ?>"></a></li>
                          <?php } ?>
                        </ul>
                      </div>
                    </div>

            <?php

                }

            endforeach;
            wp_reset_postdata();
          ?>
          </div>
        </div>
      </div>
    </div>