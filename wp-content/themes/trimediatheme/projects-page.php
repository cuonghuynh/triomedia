
<?php 
/*
Template Name: Projects Page
*/
  get_header();?>

    <?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="subpage-name-header">
				<h1><?php the_title(); ?></h1>
				<span><?php the_field('description_content'); ?></span>
			</div>
		</header>
			<div class="cus1-main">
			<div class="header-shadow"></div>
			<div class="filter">
				<?php 
					$args = array(
						'type' 		=> 'project',
						'taxonomy'	=> 'project_cat',
						'hide_empty'=> 0,
					);

					$project_categories = get_categories( $args );
					if (count($project_categories) > 0) {

						?>
					<ul class="list-filter-project">
						<li>
							<a class="btn btn-default btn-filter button is-checked" data-filter="*" href="javascript:;">Show All</a>
						</li>

						<?php
						foreach ($project_categories as $cat) {
							echo '<li>';
								echo '<a class="btn btn-default btn-filter button" data-filter=".' . $cat->slug . '" href="javascript:;">' . $cat->name . '</a>';
							echo '</li>';
						}
				?>
					</ul>

				<?php

					}
				?>
			</div>
			<div class="projects">
				<div class="list-projects isotope">
		<?php

			$posts_per_page = get_option('posts_per_page');

			$paged = 1;

			if(get_query_var('paged')){
				$paged = get_query_var('paged');
			}elseif(get_query_var('page')){
				$paged = get_query_var('page');
			}

			$args = array(
				'post_type' => array('project'),
				'pagination' => true,
				'posts_per_page' =>  $posts_per_page,
				'paged' => $paged
				);

			// if(isset($_GET['project_type'])){
			// 	$project_type =  $_GET['project_type'];
			// 	$args['category_name'] = $project_type;
			// }

			$postslist = new  WP_Query($args);

			if ( $postslist->have_posts() ) :
			while ( $postslist->have_posts() ) : $postslist->the_post(); 
				$thumb_id = get_post_thumbnail_id();
				$thumb_url = wp_get_attachment_image_src($thumb_id,'project-thumb', true);	
				
				//$cate = get_the_category();
				$terms = get_the_terms( get_the_id(), 'project_cat' );

			?>
					<div class="col-lg-3 col-md-4 col-sm-6 element-item <?php foreach ($terms as $term) {
							echo $term->slug . ' ';
						}?> " >
						<div class="intro">
							<div class="content">
								<a href="<?php the_permalink(); ?>">
									<div class="icon-right"></div>
									<div class="text">
										<h4 class="title name"><?php the_title(); ?></h4>
										<img src="<?php echo get_bloginfo('template_url'); ?>/images/dot-prj.png" />
										<p><?php the_excerpt(); ?></p>
									</div>
								</a>
							</div>
						</div>
						<div class="feature" style="background-image: url(<?php echo $thumb_url[0]; ?>)"></div>
					</div>
			
			<?php endwhile; ?>
				</div>
			</div>
			<div class="row paginator text-center">
					<ul class="col-md-12">
						<?php 

						$big = 999999;
						echo paginate_links( array(
							'base' => str_replace( $big, '%#%', esc_url( get_pagenum_link( $big ) ) ),   
							'type' => 'list',
							'format' => '?paged=%#%',
							'total' => $postslist->max_num_pages,
							'prev_text'=> 'Previous',
							'next_text'=> 'Next',
							) );

							?>
						</ul>
					</div>
				</div>
		<?php wp_reset_postdata(); ?>
	<?php endif; endwhile; endif; ?>
<?php get_footer(); ?>